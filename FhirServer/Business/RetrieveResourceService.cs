﻿using FHIR_Server.DAO;
using FhirServer.Models;
using System;

namespace FhirServer.Business
{
    public class RetrieveResourceService : IRetrieveResourceService
    {
        // Repository
        private ResourceBlobRepository  resourceBlobRepository;
        private ResourceIndexRepository resourceIndexRepository;
        private VersionHighRepository   versionHighRepository;

        /// <summary>
        /// 생성자
        /// </summary>
        public RetrieveResourceService()
        {
            resourceBlobRepository  = new ResourceBlobRepository();
            resourceIndexRepository = new ResourceIndexRepository();
            versionHighRepository   = new VersionHighRepository();
        }

        /// <summary>
        /// Logical ID 값에 해당하는 리소스 반환
        /// </summary>
        /// <param name="logicalId"></param>
        /// <returns>Resource Contents</returns>
        public byte[] retrieveResource(String logicalIdString)
        {
            byte[] returnValue = null;
            long logicalIdLong;
            long.TryParse(logicalIdString, out logicalIdLong);

            ResourceBlob foundResourceBlob = resourceBlobRepository.findTopByResourceIdOrderByVersionIdDesc(logicalIdLong);
            returnValue = foundResourceBlob.resource_contents;

            // 버전 정보 추가해야함.

            return returnValue;
        }

        /// <summary>
        /// Logical ID에 해당하는 Version ID값 반환
        /// </summary>
        /// <param name="logicalIdString"></param>
        /// <returns>Version id</returns>
        public String getVersionIdByLogicalId(String logicalIdString)
        {
            String returnValue = null;

            long logicalIdLong;
            long.TryParse(logicalIdString, out logicalIdLong);
            ResourceBlob foundResourceBlob = resourceBlobRepository.findTopByResourceIdOrderByVersionIdDesc(logicalIdLong);

            returnValue = foundResourceBlob.version_id.ToString();

            return returnValue;
        }
    }
}