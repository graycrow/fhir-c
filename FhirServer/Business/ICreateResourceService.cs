﻿using System;

namespace FhirServer.Business
{
    interface ICreateResourceService
    {
        /// <summary>
        /// Resource 생성하여 DB에 저장
        /// </summary>
        /// <param name="resourceType"></param>
        /// <param name="contents"></param>
        /// <returns></returns>
        long createResource(String resourceType, String format, byte[] contents);

        /// <summary>
        /// logical ID 값에 해당하는 ResourceBlob 정보를 데이터베이스에서 찾아 byte[] 형태로 반환
        /// </summary>
        /// <param name="logicalId"></param>
        /// <returns></returns>
        byte[] findBy(long logicalId);
    }
}
