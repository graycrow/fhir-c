﻿using FhirServer.Models;
using Hl7.Fhir.Model;
using Hl7.Fhir.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace FhirServer.Business
{
    public static class ResouceHandler
    {
        /// <summary>
        /// 포맷과 데이터를 받아 해당 포맷형식으로 파싱
        /// </summary>
        /// <param name="format"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static Resource Parse(String format, String data)
        {
            Resource resource = null;
            switch(format)
            {
                case "json":
                    resource = FhirParser.ParseResourceFromJson(data);
                    break;
                case "xml":
                    resource = FhirParser.ParseResourceFromXml(data);
                    break;
                default:
                    throw new FormatException(String.Format("Format error!"));
            }
            return resource;
        }

        /// <summary>
        /// ResourceBlob List를 번들로 묶어서 반환
        /// </summary>
        /// <param name="foundResourceBlob"></param>
        /// <returns></returns>
        public static String findByResourceBlobList(List<ResourceBlob> foundResourceBlob)
        {
            Bundle bundle = new Bundle();

            foreach(ResourceBlob resourceBlob in foundResourceBlob)
            {
                /// json 추가?
                Resource resource = FhirParser.ParseResourceFromXml(Encoding.Default.GetString(resourceBlob.resource_contents));
                //ResourceEntry resourceEntry = ResourceEntry.Create(resource);
                
               // BundleEntry bundleEntity = (BundleEntry)resourceEntry;

                //bundle.Entries.Add(bundleEntity);
            }

            //var bundleXml = FhirSerializer.SerializeBundleToXml(bundle);

            //return bundleXml;
            return null;
        }

        
    }
}