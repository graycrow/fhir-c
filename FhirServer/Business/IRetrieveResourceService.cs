﻿using System;

namespace FhirServer.Business
{
    interface IRetrieveResourceService
    {
        /// <summary>
        /// Logical ID 값에 해당하는 리소스 반환
        /// </summary>
        /// <param name="logicalId"></param>
        /// <returns>Resource Contents</returns>
        byte[] retrieveResource(String logicalIdString);

        /// <summary>
        /// Logical ID에 해당하는 Version ID값 반환
        /// </summary>
        /// <param name="logicalIdString"></param>
        /// <returns>Version id</returns>
        String getVersionIdByLogicalId(String logicalIdString);
    }
}
