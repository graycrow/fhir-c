﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FhirServer.Business
{
    interface ISearchResourceService
    {
        byte[] SearchResource(String resourceType, 
                              String label, 
                              String value, 
                              String family,
                              String given, 
                              String suffix, 
                              String gender, 
                              String birthDate,
                              String reference, 
                              String display);

        String getVersionIdByLogicalId(String logicalIdString);
    }
}
