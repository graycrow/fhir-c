﻿using System;

namespace FhirServer.Business
{
    interface IUpdateResourceService
    {
        /// <summary>
        /// Logical ID에 해당하는 리소스를 업데이트
        /// </summary>
        /// <param name="resourceType"></param>
        /// <param name="logicalId"></param>
        /// <param name="contents"></param>
        /// <returns>Logical ID(string)</returns>
        long updateResource(String resourceType, String logicalIdString, String format, byte[] contents);

        /// <summary>
        /// Logical ID에 해당하는 리소스 반환
        /// </summary>
        /// <param name="logicalId"></param>
        /// <returns>byte[]</returns>
        byte[] findBy(long logicalId);
    }
}
