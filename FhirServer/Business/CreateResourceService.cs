﻿using System;
using FhirServer.Models;
using FHIR_Server.DAO;
using Hl7.Fhir.Model;
using System.Text;

namespace FhirServer.Business
{
    public class CreateResourceService : ICreateResourceService
    {
        // Repository
        private ResourceBlobRepository    resourceBlobRepository;  
        private ResourceIndexRepository   resourceIndexRepository;
        private VersionHighRepository     versionHighRepository;

        /// <summary>
        /// 생성자
        /// </summary>
        public CreateResourceService()
        {
            resourceBlobRepository  = new ResourceBlobRepository();  
            resourceIndexRepository = new ResourceIndexRepository();
            versionHighRepository   = new VersionHighRepository();
        }

        /// <summary>
        /// Resource 생성하여 DB에 저장
        /// </summary>
        /// <param name="resourceType"></param>
        /// <param name="contents"></param>
        /// <returns></returns>
        public long createResource(String resourceType, String format, byte[] contents)
        {
            // 1. 생성할 리소스 정의
            ResourceBlob foundResourceBlob;
            Patient      patient    = new Patient();
            String       lastUpdate = DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss");

            long logical_Id = -1L;
            long row_id     = -1L;

            // 2. 포맷 형식(xml,json)에 맞게 데이터를 Patient형식으로 파싱
            try
            {
                patient = (Patient)ResouceHandler.Parse(format, Encoding.Default.GetString(contents));
            }
            catch (FormatException e) { return -1; }

            //row id
            row_id = resourceBlobRepository.Count() + 1;

            //logical id
            if (row_id > 1)
            {
                foundResourceBlob = resourceBlobRepository.findTopByOrderByResourceIdDesc();
                logical_Id = (long)foundResourceBlob.row_id + 1L;
            }
            else { logical_Id = 1L; }

            // 3. blob 리소스 생성
            ResourceBlob resourceBlob = new ResourceBlob { id = 1,
                                                           version_id = 1,
                                                           row_id = row_id,
                                                           resource_id = logical_Id,
                                                           last_update = lastUpdate,
                                                           resource_contents = contents };
            // 4. 데이터베이스 저장
            resourceBlobRepository.Save(resourceBlob);

            ///////////////////////////////////////////////////////////////////////////////////////////////////////

            // 1. 4개의 리소스인덱스(resourceBlob을 기준으로)  데이터 생성
            ResourceIndex resourceIndex = new ResourceIndex();

            if (String.Compare(resourceType, "Patient") == 0)
            {
                // 1-1. id (for search)
                resourceIndex = new ResourceIndex();
                resourceIndex.row_id = resourceBlob.id;
                resourceIndex.param = resourceType + "." + "_id";
                resourceIndex.value = patient.Identifier[0].Value;
                resourceIndexRepository.Save(resourceIndex);

                // 1-2. language 
                resourceIndex = new ResourceIndex();
                resourceIndex.row_id = resourceBlob.id;
                resourceIndex.param = resourceType + "." + "_language";
                resourceIndex.value = patient.Language;
                resourceIndexRepository.Save(resourceIndex);

                // 1-3. birthdate 
                resourceIndex = new ResourceIndex();
                resourceIndex.row_id = resourceBlob.id;
                resourceIndex.param = resourceType + "." + "birthdate";
                resourceIndex.value = patient.BirthDateElement.Value;
                resourceIndexRepository.Save(resourceIndex);

                // 1-4. namve
				String fullname = "";
				foreach(HumanName name in patient.Name)
					fullname += name.Text;
				
				resourceIndex = new ResourceIndex();
                resourceIndex.row_id = resourceBlob.id;
                resourceIndex.param = resourceType + "." + "name";
                resourceIndex.value = fullname;
                resourceIndexRepository.Save(resourceIndex);

            }
            else if (String.Compare(resourceType, "DocumentReference") == 0)
            {
                // 추후 구현 예정
            }

            ///////////////////////////////////////////////////////////////////////////////////////////////////////

            // 1. 리소스 버전을 관리하기 위한 versionHigh 생성
            VersionHigh versionHigh = new VersionHigh();
           
            versionHigh.row_id       = versionHighRepository.Count() + 1;
            versionHigh.type         = resourceType;
            versionHigh.logical_id   = logical_Id;
            versionHigh.rosource_blob_id = resourceBlob.row_id;
            versionHigh.version_id_high  = 1L;
            versionHighRepository.Save(versionHigh);

            return resourceBlob.id;
        }

        /// <summary>
        /// logical ID 값에 해당하는 ResourceBlob 정보를 데이터베이스에서 찾아 byte[] 형태로 반환
        /// </summary>
        /// <param name="logicalId"></param>
        /// <returns></returns>
        public byte[] findBy(long logicalId)
        {
            ResourceBlob body = resourceBlobRepository.findById(logicalId);
            return body.resource_contents;
        }
    }
}