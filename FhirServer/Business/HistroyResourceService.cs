﻿using FHIR_Server.DAO;
using FhirServer.Models;
using Hl7.Fhir.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FhirServer.Business
{
    public class HistroyResourceService : IHistroyResourceService
    {
        // Repository
        private ResourceBlobRepository  resourceBlobRepository;
        private ResourceIndexRepository resourceIndexRepository;
        private VersionHighRepository   versionHighRepository;

        /// <summary>
        /// 생성자
        /// </summary>
        public HistroyResourceService()
        {
            resourceBlobRepository  = new ResourceBlobRepository();
            resourceIndexRepository = new ResourceIndexRepository();
            versionHighRepository   = new VersionHighRepository();
        }

        /// <summary>
        /// Logical ID에 해당하는 모든 기록 반환
        /// </summary>
        /// <param name="logicalId"></param>
        /// <returns>Resouce Data(byte[])</returns>
        public String historyResource(String logicalIdString)
        {
            // 1. 초기화
            List<ResourceBlob> foundResourceBlob;
            Bundle bundle       = new Bundle();
            String returnValue  = null;
            
            long logicalIdLong;
            long.TryParse(logicalIdString, out logicalIdLong);

            // 2. logical ID에 해당하는 리소스들을 리스트로 가져옴
            foundResourceBlob = resourceBlobRepository.findByResourceId(logicalIdLong);

            if (foundResourceBlob.Count() <= 0)
                return null;

            // 3. 리소스들을 번들화
            returnValue = ResouceHandler.findByResourceBlobList(foundResourceBlob);

            return returnValue;
        }

        /// <summary>
        /// Logical ID에 해당하는 최신 Version ID 반환
        /// </summary>
        /// <param name="logicalIdString"></param>
        /// <returns>VersionID(String)</returns>
        public String getVersionIdByLogicalId(String logicalIdString)
        {
            String returnValue = null;

            long logicalIdLong;
            long.TryParse(logicalIdString, out logicalIdLong);
            ResourceBlob foundResourceBlob = resourceBlobRepository.findTopByResourceIdOrderByVersionIdDesc(logicalIdLong);

            returnValue = foundResourceBlob.version_id.ToString();

            return returnValue;
        }
    }
}