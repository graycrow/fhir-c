﻿using System;

namespace FhirServer.Business
{
    interface IHistroyResourceService
    {
        /// <summary>
        /// Logical ID에 해당하는 모든 기록 반환
        /// </summary>
        /// <param name="logicalId"></param>
        /// <returns>Resouce Data(byte[])</returns>
        String historyResource(String logicalId);

        /// <summary>
        /// Logical ID에 해당하는 최신 Version ID 반환
        /// </summary>
        /// <param name="logicalIdString"></param>
        /// <returns>VersionID(String)</returns>
        String getVersionIdByLogicalId(String logicalIdString);
    }
}
