﻿using FHIR_Server.DAO;
using FhirServer.Models;
using Hl7.Fhir.Model;
using System;
using System.Text;

namespace FhirServer.Business
{
    public class UpdateResourceService : IUpdateResourceService
    {
        // Repository
        private ResourceBlobRepository  resourceBlobRepository;
        private ResourceIndexRepository resourceIndexRepository;
        private VersionHighRepository   versionHighRepository;

        /// <summary>
        /// 생성자
        /// </summary>
        public UpdateResourceService()
        {
            resourceBlobRepository  = new ResourceBlobRepository();
            resourceIndexRepository = new ResourceIndexRepository();
            versionHighRepository   = new VersionHighRepository();
        }

        /// <summary>
        /// Logical ID에 해당하는 리소스를 업데이트
        /// </summary>
        /// <param name="resourceType"></param>
        /// <param name="logicalId"></param>
        /// <param name="contents"></param>
        /// <returns>Logical ID(string)</returns>
        public long updateResource(String resourceType, String logicalIdString, String format, byte[] contents)
        {
            // 1. 생성할 리소스 정의
            ResourceBlob foundResourceBlob;
            Patient      patient    = new Patient();
            String       lastUpdate = DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss");

            long returnValue = -1L;
            long version_Id;
            long logical_Id;
            long.TryParse(logicalIdString, out logical_Id);
            
            // 2. 포맷 형식(xml,json)에 맞게 데이터를 Patient형식으로 파싱
            try {
                patient = (Patient)ResouceHandler.Parse(format, Encoding.Default.GetString(contents));
            }
            catch (FormatException e) { return -1; }

            foundResourceBlob = resourceBlobRepository.findTopByResourceIdOrderByVersionIdDesc(logical_Id);
            
            version_Id = (long)foundResourceBlob.version_id + 1;

            // 3. blob 리소스 생성
            ResourceBlob resourceBlob = new ResourceBlob
            {
                version_id = version_Id,
                row_id = resourceBlobRepository.Count() + 1,
                resource_id = logical_Id,
                last_update = lastUpdate,
                resource_contents = contents
            };

            // 4. 데이터베이스 저장
            resourceBlobRepository.Save(resourceBlob);

            returnValue = resourceBlob.id;

            ///////////////////////////////////////////////////////////////////////////////////////////////////////

            // 1. foundResourceBlob ID에 해당하는 기존의 리소스 인덱스 데이터 삭제
            ResourceIndex resourceIndex = new ResourceIndex();


            if (String.Compare(resourceType, "Patient") == 0)
            {
                foreach (ResourceIndex index in resourceIndexRepository.findByRowId(foundResourceBlob.id))
                    resourceIndexRepository.Delete(index);

                // 2. 4개의 리소스인덱스(resourceBlob을 기준으로)  데이터 재 생성
                // 2-1. id (for search)
                resourceIndex = new ResourceIndex();
                resourceIndex.row_id = resourceBlob.id;
                resourceIndex.param = resourceType + "." + "_id";
                resourceIndex.value = patient.Identifier[0].Value;
                resourceIndexRepository.Save(resourceIndex);

                // 2-2. language 
                resourceIndex = new ResourceIndex();
                resourceIndex.row_id = resourceBlob.id;
                resourceIndex.param = resourceType + "." + "_language";
                resourceIndex.value = patient.Language;
                resourceIndexRepository.Save(resourceIndex);

                // 2-3. birthdate 
                resourceIndex = new ResourceIndex();
                resourceIndex.row_id = resourceBlob.id;
                resourceIndex.param = resourceType + "." + "birthdate";
                resourceIndex.value = patient.BirthDateElement.Value;
                resourceIndexRepository.Save(resourceIndex);

                // 2-4. namve
                String fullname = "";
                foreach (HumanName name in patient.Name)
                    fullname += name.Text;

                resourceIndex = new ResourceIndex();
                resourceIndex.row_id = resourceBlob.id;
                resourceIndex.param = resourceType + "." + "name";
                resourceIndex.value = fullname;
                resourceIndexRepository.Save(resourceIndex);
            }
            else if (String.Compare(resourceType, "DocumentReference") == 0)
            {
                // 추후 구현 예정
            }

            ///////////////////////////////////////////////////////////////////////////////////////////////////////

            // 1. 리소스 버전을 관리하기 위한 versionHigh 생성
            VersionHigh versionHigh = versionHighRepository.findByLogicalId(logical_Id);
            versionHighRepository.Delete(versionHigh);

            versionHigh.row_id      = versionHighRepository.Count() + 1;
            versionHigh.type        = resourceType;
            versionHigh.logical_id  = logical_Id;
            versionHigh.rosource_blob_id = resourceBlob.row_id;
            versionHigh.version_id_high++;
            versionHighRepository.Save(versionHigh);

            return returnValue;
        }

        /// <summary>
        /// Logical ID에 해당하는 리소스 반환
        /// </summary>
        /// <param name="logicalId"></param>
        /// <returns>byte[]</returns>
        public byte[] findBy(long logicalId)
        {
            ResourceBlob body = resourceBlobRepository.findById(logicalId);
            return body.resource_contents;
        }
    }
}