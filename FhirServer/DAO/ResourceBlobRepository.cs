﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FhirServer.Models;
using System.Data.Entity;
using System.Web.Mvc;

namespace FHIR_Server.DAO
{
    
    public class ResourceBlobRepository : IResourceBlobRepository
    {
        private ModelEntities db;

        public ResourceBlobRepository()
        {
            db = new ModelEntities();
        }
        /*
        public ResourceBlobRepository(ModelEntities db)
        {
            this.db = db;
        }*/

        /// <summary>
        /// resource id가 일치하는 리소스 리스트 반환
        /// </summary>
        /// <param name="resource_id"></param>
        /// <returns></returns>
        public List<ResourceBlob> findByResourceId(long resource_id)
        {
            List<ResourceBlob> resultList = new List<ResourceBlob>();

            var dbSet = db.ResourceBlob.Where(u => u.resource_id == resource_id);
            resultList = dbSet.ToList<ResourceBlob>();

            return resultList;
        }

        /// <summary>
        /// resource id 및 version id 가 일치하는 리소스 반환
        /// </summary>
        /// <param name="resource_id"></param>
        /// <param name="version_id"></param>
        /// <returns></returns>
        public ResourceBlob findByResourceIdAndVersionID(long resource_id, long version_id)
        {
            ResourceBlob resultBlob = new ResourceBlob(); // 반환 할 리소스
            
            var dbSet = db.ResourceBlob.Where(u => u.resource_id == resource_id && u.version_id == version_id);
            resultBlob = dbSet.First();
            /*
            using (ModelEntities db = new ModelEntities())
            {
                var dbSet = db.ResourceBlob.Where(u => u.resource_id == resource_id && u.version_id == version_id);
                resultBlob = dbSet.First();
                //var dbSet = db.ResourceBlob.Where(u => u.Resource_id == resource_id && u.Version_id == version_id);
                //resultBlob = (ResourceBlob)dbSet;
                var str = System.Text.Encoding.Default.GetString(resultBlob.resource_contents);

                Hl7.Fhir.Model.Resource resource = Hl7.Fhir.Serialization.FhirParser.ParseResourceFromXml(str);
            }*/
            return resultBlob;
        }

        /// <summary>
        /// id 가 일치하는 리소스 반환
        /// </summary>0
        /// <param name="id"></param>
        /// <returns></returns>
        public ResourceBlob findById(long id)
        {
            ResourceBlob resultBlob = new ResourceBlob(); // 반환 할 리소스

            var dbSet = db.ResourceBlob.Where(u => u.id == id);
            resultBlob = dbSet.First();

            return resultBlob;
        }

        /// <summary>
        /// resource id를 기준으로 역순 정렬된 데이터중 가장 위의 리소스 반환
        /// </summary>
        /// <returns></returns>
        public ResourceBlob findTopByOrderByResourceIdDesc()
        {
            ResourceBlob resultBlob = new ResourceBlob(); // 반환 할 리소스

            var dbSet = db.ResourceBlob.OrderByDescending(u => u.resource_id).Take(1);
            resultBlob = dbSet.First();

            return resultBlob;
        }

        /// <summary>
        /// version id를 기준으로 역순 정렬된 데이터중 resource id가 일치하는 가장 위의 리소스 반환
        /// </summary>
        /// <param name="logical_id"></param>
        /// <returns></returns>
        public ResourceBlob findTopByResourceIdOrderByVersionIdDesc(long logical_id)
        {
            ResourceBlob resultBlob = new ResourceBlob(); // 반환 할 리소스


            var dbSet = db.ResourceBlob.Where(u => u.resource_id == logical_id).OrderByDescending(u => u.version_id).Take(1);
            if (dbSet.Count() != 0)
                resultBlob = dbSet.First();
                
            return resultBlob;
        }

        /// <summary>
        /// 현재 repository의 리소스 개수 반환
        /// </summary>
        /// <returns></returns>
        public int Count()
        {
            int count = db.ResourceBlob.Count();

            return count;
        }

        /// <summary>
        /// 데이터베이스에 리소스 저장
        /// </summary>
        /// <param name="resourceBlob"></param>
        public void Save(ResourceBlob resourceBlob)
        {
            db.ResourceBlob.Add(resourceBlob);
            db.SaveChanges();
        }
    }
}