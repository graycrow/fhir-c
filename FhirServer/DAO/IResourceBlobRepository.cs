﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FhirServer.Models;

namespace FHIR_Server.DAO
{
    interface IResourceBlobRepository
    {
        List<ResourceBlob> findByResourceId(long resource_id);
        ResourceBlob findByResourceIdAndVersionID(long resource_id, long version_id);
        ResourceBlob findById(long id);
        ResourceBlob findTopByOrderByResourceIdDesc();
        ResourceBlob findTopByResourceIdOrderByVersionIdDesc(long logical_id);

        int  Count();
        void Save(ResourceBlob resourceBlob);
    }
}
