﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FhirServer.Models;
using System.Data.Entity;
using System.Web.Mvc;

namespace FHIR_Server.DAO
{

    public class ResourceIndexRepository : IResourceIndexRepository
    {
        private ModelEntities db;

        /// <summary>
        /// 생성자
        /// </summary>
        public ResourceIndexRepository()
        {
            db = new ModelEntities();
        }

        /// <summary>
        /// id 가 일치하는 리소스 리스트 반환
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<ResourceIndex> findById(long id)
        {
            List<ResourceIndex> resultList = new List<ResourceIndex>(); // 반환 할 리소스

            var dbSet = db.ResourceIndex.Where(u => u.id == id);
            resultList = dbSet.ToList<ResourceIndex>();

            return resultList;
        }

        /// <summary>
        /// param 이  일치하는 리소스 리스트 반환
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public List<ResourceIndex> findByParam(String param)
        {
            List<ResourceIndex> resultList = new List<ResourceIndex>(); // 반환 할 리소스

            var dbSet = db.ResourceIndex.Where(u => u.param == param);
            resultList = dbSet.ToList<ResourceIndex>();

            return resultList;
        }


        public List<ResourceIndex> findByParamAndValueStartingWith(String param, String value)
        { /* 함수 의미 파악 못했음
            List<ResourceIndex> resultList = new List<ResourceIndex>(); // 반환 할 리소스
            using (ModelEntities db = new ModelEntities())
            {
                var dbSet = db.ResourceIndex.Where(u => u.param == param && u.value);
                resultList = dbSet.ToList<ResourceIndex>();
            }
            return resultList;*/
            return null;
        }

        /// <summary>
        /// row id 가 일치하는 리소스 리스트 반환
        /// </summary>
        /// <param name="rowId"></param>
        /// <returns></returns>
        public List<ResourceIndex> findByRowId(long rowId)
        {
            List<ResourceIndex> resultList = new List<ResourceIndex>(); // 반환 할 리소스

            var dbSet = db.ResourceIndex.Where(u => u.row_id == rowId);
            resultList = dbSet.ToList<ResourceIndex>();

            return resultList;
        }

        /// <summary>
        /// 현재 repository의 리소스 개수 반환
        /// </summary>
        /// <returns></returns>
        public int Count()
        {
            int count = db.ResourceIndex.Count();

            return count;
        }

        /// <summary>
        /// 데이터베이스에 리소스 저장
        /// </summary>
        /// <param name="resourceBlob"></param>
        public void Save(ResourceIndex resourceIndex)
        {
            db.ResourceIndex.Add(resourceIndex);
            db.SaveChanges();
        }

        public void Delete(ResourceIndex resourceIndex)
        {
            db.ResourceIndex.Remove(resourceIndex);
            db.SaveChanges();
        }
    }
}