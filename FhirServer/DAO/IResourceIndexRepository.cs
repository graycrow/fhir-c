﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FhirServer.Models;

namespace FHIR_Server.DAO
{
    interface IResourceIndexRepository
    {
        List<ResourceIndex> findById(long rowId);
        List<ResourceIndex> findByParam(String param);
        List<ResourceIndex> findByParamAndValueStartingWith(String param, String value);
        List<ResourceIndex> findByRowId(long rowId);

        int  Count();
        void Save(ResourceIndex resourceIndex);
        void Delete(ResourceIndex resourceIndex);
    }
}
