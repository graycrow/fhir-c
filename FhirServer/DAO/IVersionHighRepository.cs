﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FhirServer.Models;

namespace FHIR_Server.DAO
{
    interface IVersionHighRepository
    {
        VersionHigh findByLogicalIdAndType(long logicalId, String Type);
        VersionHigh findByLogicalId(long logicalId);

        int  Count();
        void Save(VersionHigh versionHigh);
        void Delete(VersionHigh versionHigh);
    }
}
