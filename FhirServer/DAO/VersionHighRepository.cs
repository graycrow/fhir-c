﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FhirServer.Models;
using System.Data.Entity;
using System.Web.Mvc;

namespace FHIR_Server.DAO
{

    public class VersionHighRepository : IVersionHighRepository
    {
        private ModelEntities db;

        /// <summary>
        /// 생성자
        /// </summary>
        public VersionHighRepository()
        {
            db = new ModelEntities();
        }

        /// <summary>
        /// logical id와 tpye 이 일치하는 리소스 반환
        /// </summary>
        /// <param name="logicalId"></param>
        /// <param name="Type"></param>
        /// <returns></returns>
        public VersionHigh findByLogicalIdAndType(long logicalId, String type)
        {
            VersionHigh resultBlob = new VersionHigh(); // 반환 할 리소스


            var dbSet = db.VersionHigh.Where(u => u.logical_id == logicalId && u.type == type);
            resultBlob = dbSet.First();

            return resultBlob;
        }

        /// <summary>
        /// logical id가 일치하는 리소스 반환
        /// </summary>
        /// <param name="logicalId"></param>
        /// <returns></returns>
        public VersionHigh findByLogicalId(long logicalId)
        {
            VersionHigh resultBlob = new VersionHigh(); // 반환 할 리소스

            var dbSet = db.VersionHigh.Where(u => u.logical_id == logicalId);
            resultBlob = dbSet.First();

            return resultBlob;
        }

        /// <summary>
        /// 현재 repository의 리소스 개수 반환
        /// </summary>
        /// <returns></returns>
        public int Count()
        {
            int count = db.VersionHigh.Count();

            return count;
        }

        /// <summary>
        /// 데이터베이스에 리소스 저장
        /// </summary>
        /// <param name="resourceBlob"></param>
        public void Save(VersionHigh versionHigh)
        {
            db.VersionHigh.Add(versionHigh);
            db.SaveChanges();
        }
        
        /// <summary>
        /// 해당 리소스를 데이터베이스로부터 삭제
        /// </summary>
        /// <param name="versionHigh"></param>
        public void Delete(VersionHigh versionHigh)
        {
            db.VersionHigh.Remove(versionHigh);
            db.SaveChanges();
        }
    }
}