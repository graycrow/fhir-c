﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace FhirServer
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            // Create
            routes.MapRoute(
                name: "create",
                constraints: new { httpMethod = new HttpMethodConstraint(HttpMethod.Post.ToString()) },
                url: "{controller}/{resourceType}",
                defaults: new { controller = "FhirController", action = "create" }
            );

            // Read
            routes.MapRoute(
                name: "read",
                constraints: new { httpMethod = new HttpMethodConstraint(HttpMethod.Get.ToString()) },
                url: "{controller}/{resourceType}/{logical_ID}",
                defaults: new { controller = "FhirController", action = "read" }
            );

            // History
            routes.MapRoute(
                name: "history",
                url: "{controller}/{resourceType}/{logical_ID}/_history",
                constraints: new { httpMethod = new HttpMethodConstraint(HttpMethod.Get.ToString()) },
                defaults: new { controller = "FhirController", action = "history" }
            );

            // Update
            routes.MapRoute(
                name: "update",
                constraints: new { httpMethod = new HttpMethodConstraint(HttpMethod.Put.ToString()) },
                url: "{controller}/{resourceType}/{logical_ID}",
                defaults: new { controller = "FhirController", action = "update" }
            );

            // Search
            routes.MapRoute(
                name: "search",
                constraints: new { httpMethod = new HttpMethodConstraint(HttpMethod.Get.ToString()) },
                url: "{controller}/{resourceType}",
                defaults: new { controller = "FhirController", action = "search" }
            );

            // Test용. 추후 제거 //
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
