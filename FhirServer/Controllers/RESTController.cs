﻿using FhirServer.Business;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace FhirServer.Controllers
{
    [RoutePrefix("fhir")]
    public class RESTController : Controller
    {
        // GET: /fhir/
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ContentResult create(String resourceType)
        {
            ResourceHandler resourceHandler = new ResourceHandler();

            return resourceHandler.Create(Request, resourceType);
        }

        [HttpGet]
        public ContentResult read(String resourceType, String logical_ID)
        {
            ResourceHandler resourceHandler = new ResourceHandler();

            return resourceHandler.Read(Request, resourceType, logical_ID);
        }

        /*
        [HttpPost]
        public ContentResult create(String resourceType)
        {
            CreateResourceService createResourceService = new CreateResourceService();
            String format;
            String returnValue;
            
            // AcceptType 체크?
            //if (!(Request.AcceptTypes.Contains<String>("application/fhir+xml") 
            //    || Request.AcceptTypes.Contains<String>("application/xml+fhir")))
            //    return null;

            // 1-1. Content-type 체크(추후 json 추가 할것!)
            format = Request.ContentType.Split(';').First();

            if (format.CompareTo("application/fhir+xml") != 0
                && format.CompareTo("application/xml+fhir") != 0
                || Request.ContentLength == 0)
                return null;

            // 1-2. 포맷 추출
            if (format.Contains("xml") == true)
                format = "xml";
            else if (format.Contains("json") == true)
                format = "json";

            String acceptType = Request.AcceptTypes.ToList<String>().First<String>();
            Stream reqBody = Request.InputStream;

            var streamReader = new MemoryStream();
            reqBody.CopyTo(streamReader);

            // 2. 리소스 생성
            long id = createResourceService.createResource(resourceType, format, streamReader.ToArray());

            // 3. 생성된 리소스의 content 값을 byte형식으로 저장
            if (id == -1)
                returnValue = "잘못된 요청입니다";
            else
                returnValue = Encoding.UTF8.GetString(createResourceService.findBy(id));

            // 4. 생성된 리소스의 정보 리턴
            return new ContentResult
            {
                ContentType     = acceptType,
                Content         = returnValue,
                ContentEncoding = Encoding.UTF8
            };
        }

        [HttpGet]
        public ContentResult read(String resourceType, String logical_ID)
        {
            RetrieveResourceService retrieveResourceService = new RetrieveResourceService();
            byte[] contents = null;
            String returnValue;
            String contentType = Request.ContentType;

            // 1. Content-type 체크(추후 json 추가 할것!)
            if (Request.ContentType.Split(';').First().CompareTo("application/fhir+xml") != 0
                && Request.ContentType.Split(';').First().CompareTo("application/xml+fhir") != 0)
                return null;

            // 2. Logical ID에 해당하는 컨텐츠 반환
            contents = retrieveResourceService.retrieveResource(logical_ID);

            if (contents == null)
                returnValue = "잘못된 요청입니다";
            else
                returnValue = Encoding.UTF8.GetString(contents);

            //String versionIdString = retrieveResourceService.getVersionIdByLogicalId(logical_ID);

            // 3. 생성된 리소스의 정보 리턴
            return new ContentResult
            {
                ContentType = contentType,
                Content = returnValue,
                ContentEncoding = Encoding.UTF8
            };
        }

        [HttpGet]
        public ContentResult history(String resourceType, String logical_ID)
        {
            HistroyResourceService historyResourceService = new HistroyResourceService();
            String returnValue;
            String contentType = Request.ContentType;

            // 1. Content-type 체크(추후 json 추가 할것!)
            if (Request.ContentType.Split(';').First().CompareTo("application/fhir+xml") != 0
                && Request.ContentType.Split(';').First().CompareTo("application/xml+fhir") != 0)
                return null;

            // 2. Logical ID에 해당하는 리소스들을 번들화
            returnValue = historyResourceService.historyResource(logical_ID);

            if (returnValue == null)
                returnValue = "잘못된 요청입니다";

            // 버전 정보 추가

            // 3. 생성된 리소스의 정보 리턴
            return new ContentResult
            {
                ContentType = contentType,
                Content = returnValue,
                ContentEncoding = Encoding.UTF8
            };
        }

        [HttpPut]
        public ContentResult update(String resourceType, String logical_ID)
        {
            UpdateResourceService updateResourceService = new UpdateResourceService();
            String returnValue;
            String format;
            
            // AcceptType 체크?
            //if (!(Request.AcceptTypes.Contains<String>("application/fhir+xml") 
            //    || Request.AcceptTypes.Contains<String>("application/xml+fhir")))
            //    return null;

            // 1-1. Content-type 체크(추후 json 추가 할것!)
            format = Request.ContentType.Split(';').First();

            if (format.CompareTo("application/fhir+xml") != 0
                && format.CompareTo("application/xml+fhir") != 0
                || Request.ContentLength == 0)
                return null;

            // 1-2. 포맷 추출
            if (format.Contains("xml") == true)
                format = "xml";
            else if (format.Contains("json") == true)
                format = "json";

            String acceptType = Request.AcceptTypes.ToList<String>().First<String>();
            Stream reqBody = Request.InputStream;

            var streamReader = new MemoryStream();
            reqBody.CopyTo(streamReader);

            // 2. 리소스 생성
            long id = updateResourceService.updateResource(resourceType, logical_ID, format, streamReader.ToArray());

            // 3. 생성된 리소스의 content 값(byte형식)을 String 형식으로 저장
            if (id == -1)
                returnValue = "잘못된 요청입니다";
            else
                returnValue = Encoding.UTF8.GetString(updateResourceService.findBy(id));

            // 4. 생성된 리소스의 정보 리턴
            return new ContentResult
            {
                ContentType = acceptType,
                Content = returnValue,
                ContentEncoding = Encoding.UTF8
            };
        }

        [HttpGet]
        public ContentResult search(String resourceType)
        {
            SearchResourceService searchResourceService = new SearchResourceService();
            byte[] contents = null;

            String format;
            String returnValue;
            String contentType = Request.ContentType;

            String label     = Request.Params["label"];
            String value     = Request.Params["value"];
            String family    = Request.Params["family"];
            String given     = Request.Params["given"];
            String suffix    = Request.Params["suffix"];
            String gender    = Request.Params["gender"];
            String birthDate = Request.Params["birthDate"];
            String reference = Request.Params["reference"];
            String display   = Request.Params["display"];

            format = Request.ContentType.Split(';').First();

            // 1-1. Content-type 체크(추후 json 추가 할것!)
            if (format.CompareTo("application/fhir+xml") != 0
                && format.CompareTo("application/xml+fhir") != 0)
                return null;

            // 2. Logical ID에 해당하는 컨텐츠 반환
            contents = searchResourceService.SearchResource(resourceType, label, value, family, given,
                                                            suffix, gender, birthDate, reference, display);

            if (contents == null)
                returnValue = "데이터가 없습니다";
            else
                returnValue = Encoding.UTF8.GetString(contents);

            //String versionIdString = retrieveResourceService.getVersionIdByLogicalId(logical_ID);

            // 3. 생성된 리소스의 정보 리턴
            return new ContentResult
            {
                ContentType = contentType,
                Content = returnValue,
                ContentEncoding = Encoding.UTF8
            };
        }
        */
    }
}