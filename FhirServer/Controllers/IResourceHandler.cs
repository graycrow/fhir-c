﻿using System;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace FhirServer.Controllers
{
    interface IResourceHandler
    {
        String getFormat(String contentType);
        String getAcceptType(String[] acceptTypes);
        //String Read();
        //String VRead();

        ContentResult Create(HttpRequestBase httpRequestBase, String resourceType);
        ContentResult Read(HttpRequestBase httpRequestBase, String resourceType, String logical_ID);
        //ContentResult Update(HttpRequestBase httpRequestBase, String id, String resourceType);
        //HttpStatusCode Update(String id, String fromat, String data);
        //HttpStatusCode Delete(String id);
    }
}
