﻿using FhirServer.Business;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace FhirServer.Controllers
{
    public class ResourceHandler : IResourceHandler
    {
        /// <summary>
        /// contentType에서 적절한 format형식 추출
        /// </summary>
        /// <param name="contentType"></param>
        /// <returns></returns>
        public String getFormat(String contentType)
        {
            String format = null;
            String[] contentTypes = contentType.Split(';'); 

            // 1. format type 이 맞는지 확인
            foreach(String element in contentTypes)
            {
                if (element.CompareTo("application/fhir+xml") == 0 || element.CompareTo("application/xml+fhir") == 0)
                {
                    format = "xml";
                    break;
                }
                else if(element.CompareTo("application/fhir+json") == 0 || element.CompareTo("application/json+fhir") == 0)
                {
                    format = "json";
                    break;
                }
            }
            return format;
        }

        /// <summary>
        /// acceptTypes에서 적절한 형식 추출
        /// </summary>
        /// <param name="acceptTypes"></param>
        /// <returns></returns>
        public String getAcceptType(String[] acceptTypes) 
        {
            return acceptTypes.ToList<String>().First<String>();
        }

        public ContentResult Create(HttpRequestBase httpRequestBase, String resourceType)
        {
            String retStr;
            CreateResourceService createResourceService = new CreateResourceService();

            // 1. format 추출
            String format = getFormat(httpRequestBase.ContentType);

            // 2. acceptType 추출
            String acceptType = getAcceptType(httpRequestBase.AcceptTypes);

            // 3. body 추출
            Stream reqBody = httpRequestBase.InputStream;
            var streamReader = new MemoryStream();
            reqBody.CopyTo(streamReader);
            byte[] contents = streamReader.ToArray();

            // 4-1. 리소스 생성
            long id = createResourceService.createResource(resourceType, format, contents);

            // 4-2. 생성된 리소스의 content 값을 byte형식으로 저장
            if (id == -1)
                retStr = "잘못된 요청입니다";
            else
                retStr = Encoding.UTF8.GetString(createResourceService.findBy(id));

            // 4. 생성된 리소스의 정보 리턴
            return new ContentResult
            {
                ContentType     = acceptType,
                Content         = retStr,
                ContentEncoding = Encoding.UTF8
            };
        }

        public ContentResult Read(HttpRequestBase httpRequestBase, String resourceType, String logical_ID)
        {
            String retStr;
            RetrieveResourceService retrieveResourceService = new RetrieveResourceService();

            // 1. format 추출
            String format = getFormat(httpRequestBase.ContentType);

            // 2. acceptType 추출
            String acceptType = getAcceptType(httpRequestBase.AcceptTypes);

            // 3. Logical ID에 해당하는 컨텐츠 반환
            byte[] contents = retrieveResourceService.retrieveResource(logical_ID);

            if (contents == null)
                retStr = "잘못된 요청입니다";
            else
                retStr = Encoding.UTF8.GetString(contents);

            // 4. 생성된 리소스의 정보 리턴
            return new ContentResult
            {
                ContentType = acceptType,
                Content = retStr,
                ContentEncoding = Encoding.UTF8
            };
        }

        /*
        public static String read(String format )
        {
            String retStr;

            return retStr;
        }*/
    }
}